package com.semantico.interview.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import net.minidev.json.JSONObject;

/**
 * Fact entity, representing a truthful statement. Facts have categories that allow them to be grouped.
 */
@Entity
public class Fact implements Comparable<Fact>  {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private Date creationDate;
    
    private String factText;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private FactCategory factCategory;
    
    /**
     * Blank constructor for use by JPA.
     */
    public Fact() {
    }

    /**
     * Create a new Fact.
     * @param factText the fact itself, for example: "The sky is blue"
     * @param category the fact's category for example: "Geography"
     */
    public Fact(String factText, FactCategory category) {
        this.creationDate = new Date();
        this.factCategory = category;
        this.factText = factText;
    }
    
   
    
    /**
     * @return the fact's database id, may be null if the fact has not been persisted yet
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @return the date that the fact was created
     */
    public Date getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the fact itself
     */
    public String getFactText() {
        return factText;
    }
    public void setFactText(String factText) {
        this.factText = factText;
    }
    
    /**
     * @return the fact's category
     */
    public FactCategory getFactCategory() {
        return factCategory;
    }

	@Override
	public int compareTo(Fact fact) {
		return this.getFactCategory().getCategory().compareTo(fact.getFactCategory().getCategory());
		
	}
	
	
}
