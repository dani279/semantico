package com.semantico.interview.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.semantico.interview.model.Fact;
import com.semantico.interview.model.FactCategory;
import com.semantico.interview.repository.FactCategoryRepository;


@Service
@Transactional
public class FactCategoryService {


    @Autowired
    private FactCategoryRepository factCategoryRepository;
    
    public List<FactCategory> getAllFactCategories() {
        return Lists.newArrayList(this.factCategoryRepository.findAll());
    }
    
    public void delete(FactCategory fc){
    	factCategoryRepository.delete(fc);
    	
    }
    
}
