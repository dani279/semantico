package com.semantico.interview.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.semantico.interview.model.FactCategory;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.semantico.interview.model.Fact;
import com.semantico.interview.service.FactService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Controller for access to Facts.
 * Requests and responses are transmitted using JSON objects.
 */
@RestController
@RequestMapping("/api/facts")
public class FactApiController {

    @Autowired
    private FactService factService;

    /**
     * @return all of the facts in the system
     * @throws IOException 
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity  getFacts(InputStream incomingdata) throws IOException {

    	StringWriter writer = new StringWriter();
    	IOUtils.copy(incomingdata, writer, "UTF-8");
    	String theString = writer.toString();
    	JSONObject jsonObject = new JSONObject();
    	jsonObject = JSONValue.parse(theString, JSONObject.class);
        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.CREATED);
    }

    /**
     * Uncomment this code if you want to create some initial data on startup for testing endpoints.
     */
    @PostConstruct
    public void createInitialEntities() {
        this.factService.createFact(new Fact("The sky is blue", new FactCategory("science")));
        this.factService.createFact(new Fact("Grass is green", new FactCategory("science")));
        this.factService.createFact(new Fact("I think, therefore I am", new FactCategory("philosophy")));
        this.factService.createFact(new Fact("I think, but I am not sure", new FactCategory("computing")));
    }
    
    @RequestMapping(value="/display" , method = RequestMethod.GET)
    public void  display(InputStream incomingdata) throws IOException {
    	
    	createInitialEntities();
    	List<Fact> facts = this.factService.getAllFacts();
    	printCategories(facts);
    	
    }

    public void printCategories(List<Fact> facts){
    	Collections.sort(facts);
    	
    	HashMap <String, List<Fact>> categories = new HashMap<String, List<Fact>>();
    	
    	for(Fact fact : facts){

			List<Fact> factList = new ArrayList<Fact>();
    		if(categories.get(fact.getFactCategory().getCategory())!= null){
    		
    			factList = categories.get(fact.getFactCategory().getCategory());
    				
    			
    		}
			factList.add(fact);
			categories.put(fact.getFactCategory().getCategory(), factList);
    		
    	}
    	
    	JSONArray result = new JSONArray();
    	
    	Set<String> categoryList = categories.keySet();
    	
    	for (String category: categoryList){
    		
    		JSONObject  jsonObject= new JSONObject();
    		
    		jsonObject.put("category", category);
    		jsonObject.put("count", categories.get(category).size());
    		result.add(jsonObject);
    	
    	}
    	
    	System.out.println(result.toString());
    	
    }
}


